# Material for the Diagnostic Genome Analysis course

Below is a listing of all chapters and extra documents used for the course.

## Chapters & Manuals

* ###[Course Introduction](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/master/chapters/TH5_Introduction.ipynb)

    **Course Description and Background Information**  

* ###[Chapter 1; Galaxy Introduction](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/master/chapters/TH5_CH1_Galaxy_Introduction.ipynb)

    **Introducing the Galaxy Server(s)**  

* ###[Chapter 2; Data Introduction](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/master/chapters/TH5_CH2_Data.ipynb)

    **Explanation of the FASTQ Data Format**

* ###[Chapter 3; Quality Control](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/master/chapters/TH5_CH3_QC.ipynb)

    **Performing Quality Control on NGS Data**

* ###[Chapter 4; Read Mapping](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/master/chapters/TH5_CH4_Read_Mapping.ipynb)

    **Mapping NGS Data to a Reference Genome**

* ###[Chapter 5; Pileup](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/master/chapters/TH5_CH5_Pileup.ipynb)

    **Inspecting the Mapping Data**

* ###[Chapter 6; Varscan](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/master/chapters/TH5_CH6_Varscan.ipynb)

    **Finding the Actual Variant Positions**

* ###[Chapter 7; Annovar](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/master/chapters/TH5_CH7_Annovar.ipynb)

    **Annotating found Variants using ANNOVAR**

* ###[Forking and Mananaging a Repository](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/master/chapters/TH5_Repository.ipynb)

    **Instructions on Forking this Repository**

* ###[Creating a Custom Galaxy Tool](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/master/chapters/TH5_Custom_Galaxy_Tool.ipynb)

    **Instructions on How To Create a Galaxy Tool**

## Course Planning

* Week 1: *Course Introduction* & Chapters **1** through **3.2**
* Week 2: Chapters **3** through **5**, including deliverables **1** & **2** from the programming assignments (*chapter 5.2*)
* Week 3: Remaining programming assignments chapter **5** and running the `Varscan` Galaxy tool from chapter **6**
* Week 4: Programming the `Varscan` VCF-filter application (deliverable 8), creating a Galaxy VCF-filter tool (updated instructions) and running the `ANNOVAR` tool from chapter **7**
* Week 5: Writing the *Introduction* chapter, describing the `ANNOVAR` output and processing this using Python (deliverable 9)
* Week 6: Continuing programming assignments; filtering `RefSeq_Gene` data (deliverable 10), designing a database schema (deliverable 11) and creating a template for deliverable 12.
* Week 7: Finishing deliverable 12 and querying the resulting database (chapter **7.4**)