#!/usr/bin/env python3
"""
BFV2 Theme 05 - Genomics - Sequencing Project

Template for parsing and filtering VCF data given a certain variant
allele frequency value.

Deliverable 8
-------------
Make changes to the `parse_vcf_data` function AND the `main` function,
following the instructions preceded with double '##' symbols.

    usage:
        python3 deliverable8.py vcf_file.vcf frequency out_file.vcf

    arguments:
        vcf_file.vcf: the input VCF file, output from the varscan tool
                      frequency: a number (integer) to use as filtering value
        out_file.vcf: name of the output VCF file 

    output:
        a VCF file containing the complete header (comment lines) and
        only the remaining variant positions after filtering.
"""

# IMPORT
import sys
import argparse

# METADATA VARIABLES
__author__ = "Geoffrey van Gent, Wendy van der Meulen"
__status__ = "Template"
__version__ = "2017.d8.v3"


# DEFINITIONS
def parse_commandline():
    """Parses commandline arguments"""
    parser = argparse.ArgumentParser()

    # create possible arguments for argument parser
    parser.add_argument("-in", "--in_vcf", help="Place the input vcf file in here.")
    parser.add_argument("-out", "--out_vcf", help="Place the name of the output file in here.")
    parser.add_argument("-f", "--frequency", help="Place the minimum allele frequency in here.")

    # checks if enough arguments are given
    if len(sys.argv) != 7:
        parser.print_help()
        sys.exit()

    # make shortcut for given arguments
    args = parser.parse_args()

    # save arguments
    vcf_file = args.in_vcf
    output_file = args.out_vcf
    frequency = int(args.frequency)

    # return arguments
    return vcf_file, output_file, frequency


def parse_vcf_data(vcf_input_file, frequency, vcf_output_file):
    """ This function reads the input VCF file line by line, skipping the first
    n-header lines. The remaining lines are parsed to filter out variant allele
    frequencies > frequency.
    """
    with open(vcf_input_file, "r") as input_file:
        with open(vcf_output_file, "w") as output_file:
            for line in input_file:
                # write header to output file
                if line.startswith("#"):
                    output_file.write(line)
                else:
                    columns = line.split("\t")
                    sample = columns[9]
                    freq = float(sample.split(":")[6].replace("%", ""))
                    if freq > frequency:
                        output_file.write(line)


# MAIN
def main():
    """ Main function """
    # PREPARATIONS
    if len(sys.argv) > 1:
        args = parse_commandline()
        vcf_file = args[0]
        out_vcf = args[1]
        frequency = args[2]
    else:
        print('Warning, no arguments given, using default values (testing only)...')
        vcf_file = 'data/example.vcf'
        out_vcf = 'data/d8_output.vcf'
        frequency = 30

    # WORK
    parse_vcf_data(vcf_file, frequency, out_vcf)

    # FINISH
    return 0

# RUN
if __name__ == "__main__":
    sys.exit(main())
