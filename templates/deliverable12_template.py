#!/usr/bin/env python3
"""
Program that puts chromosomes, gene and variant data
in a database of the users choosing.
    Usage: python3 deliverable12_template.py
                                    -d database_name
                                    -u username
                                    -p password
                                    -a annovar_file
                                    [-s sql database schema file]

    Return: database with information in the annovar_file,
            rapport about the size of the tables in the database.
"""

# IMPORTS
import argparse
import sys
import database_management.py

# METADATA
__author__ = "Geoffrey van Gent, Wendy van der Meulen"
__version__ = "0.1"
__status__ = "template"

# GLOBAL VARIABLES


# CLASSES
class Gene:
    """Class that includes all the processes
    which are needed for the gene information."""
    def __init__(self, name, chromosome):
        pass

    def __str__(self):
        """Printed representation of the gene"""
        return "gene representation"


class Variant:
    """Class that has all the information about variants."""
    def __init__(self, gene, begin, end,
                 reference_base,
                 observed_base, function_base,
                 bdsnp128="NA", sift="NA",
                 polyphen="NA", clinvar="NA"):
        self.gene = self.get_gene_name(gene)

    def get_gene_name(self, gene_name):
        """
        This function returns the gene name from a complex 'RefSeq_Gene' field
        from the ANNOVAR output.
        'LOC****' (Uncharacterized Locus),
        'LINC****' (Long Intergenic Non-Coding RNA segments) and
        'NONE' elements are filtered out.

        If a record contains multiple gene-names (incase of an intergenic variant),
        combine these genes with a '/' delimeter, i.e.: 'BIN1/CYP27C1'

        If a record only contains 'NONE', 'LOC' or 'LIN' elements, the gene name
        becomes a '-'.

        Input is a single RefSeq_Gene record (String) and the output is a single
        string with the filtered gene name.
        """
        pass
        return "gene_name"

    def __str__(self):
        """writes string representation of the variant"""
        return "string rep variant (deliverable 9 function)"


# FUNCTIONS
def parse_commandline():
    """Parses commandline arguments"""
    pass
    # return args = database_name, username, password, annovar_file (, sql_file)]


def parse_annovar_data(annovar_file):
    """Extracts useful columns from annovar file."""
    return 0  # variant_list
    pass


def check_database(db, username, password):
    """Checks if the given database credentials are useful"""
    pass
    return True


def check_file(file):
    """Checks if a file exists and can be opened"""
    pass
    return True


def extract_genes(variants):
    """Extracts all the genes from the variant objects"""
    pass


def extract_chromosomes():
    """Extract all chromosomes from the gene objects"""
    chromosome_list = []
    return chromosome_list


# MAIN
def main():
    """Takes the program through it's functions"""
    # PREPARATIONS
    args = parse_commandline()
    # if credentials aren't useful, exit program
    check_database(args.database, args.username, args.password)
    # if unknown file, exit program
    check_file(args.annovar)
    sql_file = False
    if len(args) == 5:
        sql_file = args[4]

    # WORK
    # extract data from files
    variants = parse_annovar_data(args.annovar)
    genes = extract_genes(variants)
    chromosomes = extract_chromosomes()
    # write the data to the database
    database_writer = database_management.WriteToDatabase(args.database, args.username, args.password, chromosomes, genes, variants, sql_file)

    # FINISH
    print(database_writer)
    return 0

if __name__ == '__main__':
    sys.exit(main())
