#!/usr/bin/env python3
"""
BFV2 Theme 05 - Genomics - Sequencing Project

Template for parsing and filtering VCF data given a certain variant
allele frequency value.

Deliverable 9
-------------

    usage:
        python3 deliverable9.py -a annovar_file

    arguments:
        annovar_file: name of the input annovar file
"""

# IMPORT
import sys
import argparse
from operator import itemgetter

# METADATA VARIABLES
__author__ = "Geoffrey van Gent, Wendy van der Meulen"
__status__ = "Template"
__version__ = "2017.d8.v3"


# DEFINITIONS
def parse_commandline():
    """Parses commandline arguments"""
    parser = argparse.ArgumentParser()

    # create possible arguments for argument parser
    parser.add_argument("-a", "--annovar", help="Place the input annovar file in here.")

    # checks if enough arguments are given
    if len(sys.argv) != 3:
        parser.print_help()
        sys.exit()

    # make shortcut for given arguments
    args = parser.parse_args()

    # save arguments
    annovar_file = args.annovar

    # return arguments
    return annovar_file


def extract_useful_columns(annovar_file):
    """Extracts useful columns from annovar file."""
    useful_data = []
    with open(annovar_file) as open_file:
        for line in open_file:
            split_line = line.strip("\n").split('\t')
            columns = [16, 0, 1, 2, 3, 4, 15, 16, 27, 34, 35, 53]
            variant = itemgetter(*columns)(split_line)
            useful_data.append(list(variant))

    return useful_data


def print_data(useful_data):
    """Prints for every variant the useful data."""
    header = useful_data[0][6:]
    for variant in useful_data:
        if not variant[6:] == header:
            print("'" + "', '".join(variant[:6]) + "'")
            for i in range(6):
                print("\t{}: {}".format(header[i], variant[6:][i]))
            print("")
    return 0


# MAIN
def main():

    # PREPARATIONS
    annovar_file = parse_commandline()

    # WORK
    useful_data = extract_useful_columns(annovar_file)

    # FINISH
    print_data(useful_data)
    return 0

if __name__ == '__main__':
    sys.exit(main())
