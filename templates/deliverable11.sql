-- drop the tables if they already exist
drop table if exists variants;
drop table if exists genes;
drop table if exists chromosomes;

-- create tables
create table chromosomes(
    name varchar(20) not null unique,

    primary key(name)
);

create table genes(
    id int not null unique auto_increment,
    name varchar(50) not null,
    chromosome varchar(20) not null,

    primary key(id),
    foreign key(chromosome)
		references chromosomes(name) on delete restrict
);


create table variants(
    id int not null unique auto_increment,
    begin int not null,
    end int not null,
    reference_base char(1) not null,
    observed_base char(1) not null,
    gene int not null,
    rsfunction varchar(30) not null,
    dbsnp128 varchar(15),
    LJB2_SIFT float,
    LJB2_PolyPhen2_HDIV varchar(30),
    CLINVAR varchar(250),

    foreign key(gene)
		references genes(id) on delete restrict,
    primary key(id)
);

-- test insert into tables
insert into chromosomes values('chr1');
insert into genes(name, chromosome) values('NBPF1', 'chr1');
insert into variants(begin, end, reference_base, observed_base, gene, rsfunction, dbsnp128, LJB2_SIFT, LJB2_PolyPhen2_HDIV, CLINVAR) values(16888422, 16888422, 'T', 'G', 1, 'downstream', 'rs3868812', Null, Null, Null);
