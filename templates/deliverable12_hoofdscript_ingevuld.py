#!/usr/bin/env python3
"""
Program that puts chromosomes, gene and variant data
in a database of the users choosing.
    Usage: python3 deliverable12_template.py
                                    -d database_name
                                    -u username
                                    -p password
                                    -a annovar_file
                                    [-s sql database schema file]

    Return: database with information in the annovar_file,
            rapport about the size of the tables in the database.
"""

# IMPORTS
import argparse
import re
import sys
from operator import itemgetter
import mysql.connector
from mysql.connector import errorcode
import database_management

# METADATA
__author__ = "Geoffrey van Gent, Wendy van der Meulen"
__version__ = "0.1"
__status__ = "template"

# GLOBAL VARIABLES


# CLASSES
class Gene:
    """Class that includes all the processes
    which are needed for the gene information."""
    def __init__(self, name, chromosome):
        self.name = name
        self.chromosome = chromosome

    def __str__(self):
        """Printed representation of the gene"""
        return "De naam van dit gen is: {}\nDit gen ligt op chromosome {}".format(self.name, self.chromosome)


class Variant:
    """Class that has all the information about variants."""
    def __init__(self, gene, chromosome, begin, end,
                 reference_base,
                 observed_base, function_base,
                 dbsnp138, sift,
                 polyphen, clinvar):
        self.gene_name = gene
        self.gene = self.get_gene_name()
        self.chromosome = chromosome
        self.begin = int(begin)
        self.end = int(end)
        self.ref_base = reference_base
        self.ob_base = observed_base
        self.rsfunction = function_base
        self.dbsnp = dbsnp138
        if sift:
            self.sift = sift
        else:
            self.sift = 'Null'
        if polyphen:
            self.polyphen = polyphen
        else:
            self.polyphen = 'Null'
        if clinvar:
            self.clinvar = clinvar
        else:
            self.clinvar = 'Null'
        if dbsnp138:
            self.dbsnp = dbsnp138
        else:
            self.dbsnp = 'Null'

    def get_parts_gene_name(self):
        """Get's the different parts in a gene name"""
        split = self.gene_name.split(",")
        parts = []
        idx = 0
        real = 0
        for counter, part in enumerate(split):
            if part.endswith(")") and "(" not in part:
                if real == 0:
                    parts = list()
                    parts.append(",".join(split[:counter + 1]))
                    real = 1
                    idx = counter
                else:
                    parts = parts[:real]
                    parts.append(",".join(split[idx:counter + 1]))
            else:
                parts.append(part)

        return parts

    def get_gene_name(self):
        """
        This function returns the gene name from a complex 'RefSeq_Gene' field
        from the ANNOVAR output.
        'LOC****' (Uncharacterized Locus),
        'LINC****' (Long Intergenic Non-Coding RNA segments) and
        'NONE' elements are filtered out.

        If a record contains multiple gene-names (incase of an intergenic variant),
        combine these genes with a '/' delimeter, i.e.: 'BIN1/CYP27C1'

        If a record only contains 'NONE', 'LOC' or 'LIN' elements, the gene name
        becomes a '-'.

        Input is a single RefSeq_Gene record (String) and the output is a single
        string with the filtered gene name.
        """
        gene = []
        # Process ...
        pattern = re.compile('(\(.+\))')
        parts = self.get_parts_gene_name()
        for part in parts:
            if "NONE" in part[:4] or "LOC" in part[:3] or "LINC" in part[:4] or "RNA" in part[:3]:
                gene.append("-")
            else:
                match = pattern.search(part)
                if match:
                    for group in match.groups():
                        gene.append(part.replace(group, ""))
                else:
                    gene.append(part)
        if len(gene) > 1 and "-" in gene and set(gene) != set("-"):
            gene.remove("-")
        elif set(gene) == set("-"):
            gene = ["-"]

        return "/".join(gene)

    def __str__(self):
        """writes string representation of the variant"""
        return "{0}, {1}, {2}, {3}, {4}{5}Function: {6}{5}Gene: {0}{5}dbsnp138: {7}{5}LJB2_SIFT: {8}" \
               "{5}LJB2_PolyPhen2_HDIV: {9}{5}Clinvar: {10}\n".format(self.gene, self.begin, self.end,
                                                                      self.ref_base, self.ob_base, "\n\t",
                                                                      self.rsfunction, self.dbsnp, self.sift,
                                                                      self.polyphen, self.clinvar)


# FUNCTIONS
def parse_commandline():
    """Parses commandline arguments"""
    parser = argparse.ArgumentParser()

    # create possible arguments for argument parser
    parser.add_argument("-d", "--db_name", help="Place the database name in here.")
    parser.add_argument("-u", "--username", help="Place here your username for the database.")
    parser.add_argument("-p", "--password", help="place your password for your database account in here.")
    parser.add_argument("-a", "--annovar", help="place the annovar file in here.")
    parser.add_argument("-s", "--sql_file", help="sql database schema file")

    # checks if enough arguments are given
    if len(sys.argv) != 11:
        parser.print_help()
        sys.exit()

    # parse given arguments
    args = parser.parse_args()

    return args


def parse_annovar_data(annovar_file):
    """Extracts useful columns from annovar file."""
    variants_list = []
    header = ['RefSeq_Gene', 'chromosome', 'begin',
              'end', 'reference', 'observed',
              'RefSeq_Func', 'dbsnp138', 'LJB2_SIFT',
              'LJB2_PolyPhen2_HDIV', 'CLINVAR']
    with open(annovar_file) as open_file:
        for line in open_file:
            split_line = line.strip("\n").split('\t')
            columns = [16, 0, 1, 2, 3, 4, 15, 27, 34, 35, 53]
            variant = itemgetter(*columns)(split_line)
            if list(variant) != header:
                variants_list.append(Variant(*list(variant)))

    return variants_list


def check_database(database, username, password):
    """Checks if the given database credentials are useful"""
    try:
        cnx = mysql.connector.connect(user=username,
                                      database=database,
                                      password=password,
                                      host='mysql.bin')
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password, exiting now...")
            sys.exit()
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist, exiting now...")
            sys.exit()
        else:
            print(err, "exiting now...")
            sys.exit()
    else:
        cnx.close()

    return 0


def check_file(file):
    """Checks if a file exists and can be opened"""
    try:
        with open(file, "r"):
            print("checking file: {}".format(file))
    except FileNotFoundError:
        print("This file, {}, doesn't exist, exiting now...".format(file))
        sys.exit(1)

    return 0


def extract_genes(variants):
    """Extracts all the genes from the variant objects"""
    genes = []
    for gene_info in set([(variant.gene, variant.chromosome) for variant in variants]):
        genes.append(Gene(*gene_info))

    return genes


def extract_chromosomes(genes):
    """Extract all chromosomes from the gene objects"""
    chromosome_list = []
    for gene in genes:
        chromosome_list.append(gene.chromosome)

    return list(set(chromosome_list))


# MAIN
def main():
    """Takes the program through it's functions"""
    # PREPARATIONS
    args = parse_commandline()
    # if credentials aren't useful, exit program
    # check_database(args.db_name, args.username, args.password)
    # if unknown file, exit program
    check_file(args.annovar)
    check_file(args.sql_file)

    # WORK
    # extract data from files
    variants = parse_annovar_data(args.annovar)
    genes = extract_genes(variants)
    chromosomes = extract_chromosomes(genes)
    # write the data to the database
    database_writer = database_management.WriteToDatabase(args.db_name, args.username,
                                                          args.password, chromosomes,
                                                          genes, variants, args.sql_file)

    # FINISH
    print(database_writer)

    return 0

if __name__ == '__main__':
    sys.exit(main())
