#!/usr/bin/env python3
"""
Module that writes chromosome data to a mySQL database.
    Usage: import database_management,
            database_management.WriteToDatabase(db_name,
                                                username,
                                                password,
                                                chromosomes,
                                                genes,
                                                variants,
                                                sql_file)
"""
# IMPORTS
import mysql.connector

# METADATA
__author__ = "Geoffrey van Gent, Wendy van der Meulen"
__version__ = "0.1"
__status__ = "template"


# CLASSES
class WriteToDatabase:
    """Class that uses mySQL connect
    to create a fully filled database"""
    def __init__(self, db_name, username,
                 password, chromosomes,
                 genes, variants, sql_file):
        self.cnx = mysql.connector.connect(user=username,
                                           database=db_name,
                                           password=password,
                                           host='mysql.bin')
        self.cursor = self.cnx.cursor()
        self.sql_file = sql_file
        self.sql = self.parse_sql_file()
        self.chromosomes = chromosomes
        self.genes = genes
        self.variants = variants

        # functions
        self.create_tables()
        self.write_chromosomes_table()
        self.write_genes_table()
        self.write_variants_table()

    def parse_sql_file(self):
        """Parses sql file"""
        lines = []
        table = []
        with open(self.sql_file) as open_file:
            for line in open_file:
                if line.startswith("--"):
                    continue
                if line.startswith("create table"):
                    lines.append(" ".join(table))
                    table = list()
                    table.append(line.replace("\n", "").strip())
                elif line.startswith("insert into"):
                    lines.append(" ".join(table))
                    break
                elif line.startswith("drop table if exists"):
                    lines.append(line.replace("\n", "").strip())
                elif line.replace("\n", "").strip():
                    table.append(line.replace("\t", "").replace("\n", "").strip())

        return lines

    def create_tables(self):
        """Creates tables in database
        with or without an sql file"""
        for line in self.sql:
            if not line.startswith("--"):
                self.cursor.execute(line)

        self.cnx.commit()

    def write_chromosomes_table(self):
        """Writes data to chromosome table"""
        insert = "insert into chromosomes values('{}');"
        for chromosome in self.chromosomes:
            self.cursor.execute(insert.format(chromosome))
        self.cnx.commit()

    def write_genes_table(self):
        """Writes data to gene table"""
        for gene in self.genes:
            insert = "insert into genes(name, chromosome) values('{}', '{}');"
            self.cursor.execute(insert.format(gene.name, gene.chromosome))
        self.cnx.commit()

    def write_variants_table(self):
        """Writes data to variant table"""
        keys = {'ref_base': 'reference_base', 'ob_base': 'observed_base',
                'clinvar': 'CLINVAR', 'polyphen': 'LJB2_PolyPhen2_HDIV',
                'dbsnp': 'dbsnp128', 'sift': 'LJB2_SIFT'}
        for variant in self.variants:
            insert = "insert into variants(1_k, 2_k, 3_k, " \
                 "4_k, 5_k, 6_k, 7_k, 8_k, 9_k, 10_k) " \
                 "values(1_v, 2_v, 3_v, 4_v," \
                 "5_v, 6_v, 7_v, 8_v, 9_v, 10_v);"
            self.cursor.execute("select id from genes where name = '{}' "
                                "and chromosome = '{}' limit 0, 1;"
                                .format(variant.gene, variant.chromosome))
            gene = int(self.cursor.fetchone()[0])
            var_dict = vars(variant)
            # remove instance variables which don't have to go in the table
            var_dict.__delitem__('chromosome')
            var_dict.__delitem__('gene_name')
            i = 1
            for key, value in zip(var_dict.keys(), var_dict.values()):
                # conduct table name
                try:
                    table_name = keys[key]
                except KeyError:
                    table_name = key

                # place correct value in insert statement
                if table_name == "gene":
                    insert = insert.replace(str(i) + "_v", str(gene))
                elif isinstance(value, str):
                    insert = insert.replace(str(i) + "_v", "'{}'").format(str(value))
                else:
                    insert = insert.replace(str(i) + "_v", str(value))

                # place correct key in insert statement
                insert = insert.replace(str(i) + "_k", table_name)
                i += 1

            self.cursor.execute(insert.replace("'Null'", "Null"))
        self.cnx.commit()

    def __str__(self):
        """prints how many rows per table have been written."""
        self.cursor.execute("select count(*) from chromosomes;")
        chromosomes = int(self.cursor.fetchone()[0])
        self.cursor.execute("select count(*) from genes;")
        genes = int(self.cursor.fetchone()[0])
        self.cursor.execute("select count(*) from variants;")
        variants = int(self.cursor.fetchone()[0])
        return "Amount of rows written to chromosomes table: {0}{1}" \
               "Amount of rows written to genes table: {2}{1}" \
               "Amount of rows written to variants table: {3}."\
            .format(chromosomes, "\n", genes, variants)
