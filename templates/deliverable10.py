#!/usr/bin/env python3

"""
BFV2 Theme 05 - Genomics - Sequencing Project

Template for filtering Gene names from the 'RefSeq_Gene' column given
in ANNOVAR output files.

Deliverable 10
-------------
Make changes to the 'get_gene_name' function

    usage:
        python3 deliverable10.py
"""
# IMPORT
import sys
import re

# METADATA VARIABLES
__author__ = "Geoffrey van Gent, Wendy van der Meulen"
__status__ = "Template"
__version__ = "2017.d10.v2"


# DEFINITIONS
def get_gene_name(gene_name):
    """
    This function returns the gene name from a complex 'RefSeq_Gene' field
    from the ANNOVAR output.
    'LOC****' (Uncharacterized Locus),
    'LINC****' (Long Intergenic Non-Coding RNA segments) and
    'NONE' elements are filtered out. The LOC and LINC elements can be searched
    on http://rnacentral.org/ for further information.

    If a record contains multiple gene-names (incase of an intergenic variant),
    combine these genes with a '/' delimeter, i.e.: 'BIN1/CYP27C1'

    If a record only contains 'NONE', 'LOC' or 'LIN' elements, the gene name
    becomes a '-'.

    Input is a single RefSeq_Gene record (String) and the output is a single
    string with the filtered gene name. Review the input/output examples in
    the main function below and execute this program before making any changes.
    """

    gene = []
    # Process ...
    p = re.compile('(\(.+\))')
    split = gene_name.split(",")
    parts = []
    idx = 0
    real = 0
    for counter, part in enumerate(split):
        if part.endswith(")") and "(" not in part:
                if real == 0:
                    parts = list()
                    parts.append(",".join(split[:counter+1]))
                    real = 1
                    idx = counter
                else:
                    parts = parts[:real]
                    parts.append(",".join(split[idx:counter + 1]))
        else:
            parts.append(part)
    for part in parts:
        if "NONE" in part[:4] or "LOC" in part[:3] or "LINC" in part[:4] or "RNA" in part[:3]:
            gene.append("-")
        else:
            m = p.search(part)
            if m:
                for group in m.groups():
                    if group:
                        gene.append(part.replace(group, ""))
            else:
                gene.append(part)
    if len(gene) > 1 and "-" in gene and set(gene) != set("-"):
        gene.remove("-")
    elif set(gene) == set("-"):
        gene = "-"

    return "/".join(gene)


######
# Do not change anything below this line
######

# MAIN
def main(args):
    """ Main function """

    ### INPUT ###
    refseq_genes = ['NEXN(NM_001172309:c.*22C>G,NM_144573:c.*22C>G),TNNI3(NM_000363:exon5:c.371+2T>A)',
                    'TSHZ3(dist=65732),THEG5(dist=173173)',
                    'ACTR3BP2(dist=138949),NONE(dist=NONE)',
                    'BIN1(dist=32600),CYP27C1(dist=43909)',
                    'LOC101927282(dist=1978702),LOC101927305(dist=14658)',
                    'NBPF10,NBPF20',
                    'ERBB4',
                    'LOC100507291',
                    'NONE']

    ### OUTPUT ###
    genes = ['NEXN/TNNI3',
        'TSHZ3/THEG5',
        'ACTR3BP2',
        'BIN1/CYP27C1',
        '-',
        'NBPF10/NBPF20',
        'ERBB4',
        '-',
        '-'
    ]
    # Process the ANNOVAR-file
    fail = 0
    for i, refseq_gene in enumerate(refseq_genes):
        filtered_gene = get_gene_name(refseq_gene)
        print("Input RefSeq_Gene: '", refseq_gene, "', Filtered gene name: '",
              filtered_gene, "'", sep='')
        if filtered_gene != genes[i]:
            print("\tUnfortunately, '", filtered_gene,
                  "' (your output) is different from the expected output ('",
                  genes[i], "').\n", sep='')
            fail = 1
        else:
            print('\tWell done! The gene name is correct.')

    if fail == 1:
        print('\nNot all genes are filtered correctly, please review',
              'the list above and try again.')
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
